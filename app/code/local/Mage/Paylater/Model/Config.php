<?php

class Mage_Paylater_Model_Config extends Varien_Object
{
    const MODE_TEST         = 'TEST';
    const MODE_LIVE         = 'LIVE';

    const PAYMENT_TYPE_PAYMENT      = 'PAYMENT';
    const PAYMENT_TYPE_DEFERRED     = 'DEFERRED';
    const PAYMENT_TYPE_AUTHENTICATE = 'AUTHENTICATE';
    const PAYMENT_TYPE_AUTHORISE    = 'AUTHORISE';


    /**
     *  Return config var
     *
     *  @param    string Var key
     *  @param    string Default value for non-existing key
     *  @return	  mixed
     */
    public function getConfigData($key, $default=false)
    {
        if (!$this->hasData($key)) {
             $value = Mage::getStoreConfig('payment/paylater_standard/'.$key);
             if (is_null($value) || false===$value) {
                 $value = $default;
             }
            $this->setData($key, $value);
        }
        return $this->getData($key);
    }


    /**
     *  Return Store description sent to Paylater
     *
     *  @return	  string Description
     */
    public function getDescription ()
    {
        return $this->getConfigData('description');
    }

    /**
     *  Return Paylater Mert Id
     *
     *  @return	  string Paylater Mert Id
     */
    public function getPaylaterMertId ()
    {
        return $this->getConfigData('paylater_mert_id');
    }



    /**
     *  Return Paylater Gway Name
     *
     *  @return	  string Paylater Gway Name
     */
    public function getPaylaterGwayName ()
    {
        return $this->getConfigData('paylater_gway_name');
    }

	/**
     *  Return Paylater Gway Name
     *
     *  @return	  string Paylater Secret Key
     */
    public function getPaylaterMacKey ()
    {
        return $this->getConfigData('paylater_mac_key');
    }
	

	/**
     *  Return Paylater Gway First
     *
     *  @return	  string Paylater Gway First
     */
    public function getPaylaterGwayFirst ()
    {
        return $this->getConfigData('paylater_gway_first');
    }

		
	
    /**
     *  Return working mode (see SELF::MODE_* constants)
     *
     *  @return	  string Working mode
     */
    public function getMode ()
    {
        return $this->getConfigData('mode');
    }

    /**
     *  Return new order status
     *
     *  @return	  string New order status
     */
    public function getNewOrderStatus ()
    {
        return $this->getConfigData('order_status');
    }

    /**
     *  Return debug flag
     *
     *  @return	  boolean Debug flag (0/1)
     */
    public function getDebug ()
    {
        return $this->getConfigData('debug_flag');
    }

	/**
     *  Return Action URL
     *
     *  @return	  Action URL
     */
    public function getActionUrl ()
    {
        $url = 'https://sandbox.paylater.ng/gateway';  
		return $url;
    }

    /**
     * 
     * @return type
     */
    public function getVendorId(){
        if($this->getConfigData('vendor_id')== null || $this->getConfigData('vendor_id')==''){
        return "905447976";
        
        }
        else{
            return $this->getConfigData('vendor_id');
        }
    }
    public function getVendor(){
        
        if($this->getConfigData('vendor')== null || $this->getConfigData('vendor')==''){
        return "TestVendor";
        
        }
        else{
            return $this->getConfigData('vendor');
        }
        
    }





}