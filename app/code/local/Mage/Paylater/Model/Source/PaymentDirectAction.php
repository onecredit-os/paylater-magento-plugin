<?php

class Mage_Paylater_Model_Source_PaymentDirectAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'Yes', 'label' => Mage::helper('paylater')->__('Yes')),
            array('value' => 'No', 'label' => Mage::helper('paylater')->__('No')),
            
        );
    }
}