<?php

class Mage_Paylater_Block_Standard_Redirect extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
        $standard = Mage::getModel('paylater/standard');
        $form = new Varien_Data_Form();
        $form->setAction($standard->getPaylaterUrl())
            ->setId('paylater_standard_checkout')
            ->setName('paylater_standard_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($standard->setOrder($this->getOrder())->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }
        
        $html = '<html><body>
        ';
        $html.= $this->__('You will be redirected to Paylater in a few seconds.');
        $html.= $form->toHtml();
        $html.= '<script type="text/javascript">document.getElementById("paylater_standard_checkout").submit();</script>';
        $html.= '</body></html>';

        return $html;
    }
}